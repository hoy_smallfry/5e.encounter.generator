﻿using System;
using System.Collections.Generic;

struct CreatureCategory
{
    #region Static Methods
    public static long GetXP(double cr)
    {
        CreatureCategory category = Constants.GetCategory(cr);

        return category.ExperiencePoints;
    }

    public static string ToString(CreatureCategory category)
    {
        return $"CR {category.ChallengeRating} = {category.ExperiencePoints} XP";
    }
    #endregion

    #region Methods
    public CreatureCategory(float cr, uint xp)
    {
        ChallengeRating = cr;
        ExperiencePoints = xp;
    }
    #endregion

    #region Properties
    public float ChallengeRating { get; private set; }

    public uint ExperiencePoints { get; private set; }
    #endregion
}

struct CondensedCreatures
{
    #region Static Methods
    public static long GetXP(CondensedCreatures condensedCreatures)
    {
        return CreatureCategory.GetXP(condensedCreatures.Category) * condensedCreatures.Count;
    }

    public static long GetXPMultiplied(CondensedCreatures condensedCreatures)
    {
        long xp = GetXP(condensedCreatures);

        return Constants.GetXP(xp, condensedCreatures.Count);
    }

    public static string ToString(CondensedCreatures condensedCreatures, string name = null)
    {
        var category = Constants.GetCategory(condensedCreatures.Category);

        if (name != null)
        {
            return $"{condensedCreatures.Count} * {name}";
        }

        return $"{condensedCreatures.Count} * ({CreatureCategory.ToString(category)})";
    }
    #endregion

    #region Methods
    public CondensedCreatures(double category, long count)
    {
        Category = category;
        Count = count;
    }

    public CondensedCreatures(Dictionary<string, object> dictionary)
    {
        object category = dictionary["Category"];

        Type type = category.GetType();
        if (type == typeof(double))
        {
            Category = (double)category;
        }
        else if (type == typeof(long))
        {
            Category = (long)category;
        }
        else
        {
            Category = 0;
        }

        Count = (long)dictionary["Count"];
    }
    #endregion

    #region Properties
    public double Category
    {
        get;
        private set;
    }

    public long Count
    {
        get;
        set;
    }
    #endregion
}

struct CreatureEncounter
{
    #region Static Methods
    public static long GetXP(CreatureEncounter creatureEncounter)
    {
        long xp = 0;
        int count = 0;

        for (int i = 0; i < creatureEncounter.Creatures.Count; ++i)
        {
            CondensedCreatures condensed = creatureEncounter.Creatures[i];

            xp += CondensedCreatures.GetXP(condensed);

            count += (int)condensed.Count;
        }

        return Constants.GetXP(xp, count);
    }

    public static string ToString(CreatureEncounter creatureEncounter,
        List<string> categorizedNames = null)
    {
        int count = 0;
        var condensedCreatures = creatureEncounter.Creatures;

        string[] condensedStrs = new string[condensedCreatures.Count];
        for (int i = 0; i < condensedCreatures.Count; ++i)
        {
            CondensedCreatures condensed = condensedCreatures[i];

            string condensedStr;
            if (categorizedNames != null)
            {
                condensedStr = CondensedCreatures.ToString(
                    condensed, categorizedNames[i]);
            }
            else
            {
                condensedStr = CondensedCreatures.ToString(condensed);
            }
            
            condensedStrs[i] = $"({condensedStr})";

            count += (int)condensed.Count;
        }

        string sumStr = string.Join(" + ", condensedStrs);

        float multiplier = Constants.GetMultiplier(count);

        return $"{multiplier} * ({sumStr}) = {GetXP(creatureEncounter)}";
    }
    #endregion

    #region Methods
    public CreatureEncounter(List<CondensedCreatures> enemies)
    {
        Creatures = enemies;
    }

    public CreatureEncounter(List<object> array)
    {
        Creatures = new List<CondensedCreatures>(array.Count);

        for (int i = 0; i < array.Count; ++i)
        {
            var item = array[i] as Dictionary<string, object>;
            Creatures.Add(new CondensedCreatures(item));
        }
    }
    #endregion

    #region Properties
    public List<CondensedCreatures> Creatures
    {
        get;
        private set;
    }
    #endregion
}

struct CondensedCharacters
{
    #region Static Methods
    public static long[] GetXP(CondensedCharacters creatureEncounter)
    {
        int level = (int)creatureEncounter.CharacterLevel - 1;

        var xp = new long[4];
        for (int j = 0; j < 4; ++j)
        {
            xp[j] += Constants.xpByDifficultyByPlayerLevel[level][j] * creatureEncounter.Count;
        }

        return xp;
    }
    #endregion

    #region Methods
    public CondensedCharacters(long characterLevel, long count)
    {
        CharacterLevel = characterLevel;
        Count = count;
    }

    public CondensedCharacters(Dictionary<string, object> dictionary)
    {
        object characterLevel = dictionary["Level"];
        System.Type type = characterLevel.GetType();
        if (type == typeof(double))
        {
            CharacterLevel = (long)(double)characterLevel;
        }
        else if (type == typeof(long))
        {
            CharacterLevel = (long)characterLevel;
        }
        else
        {
            CharacterLevel = 0;
        }

        Count = (long)dictionary["Count"];
    }
    #endregion

    #region Properties
    public long CharacterLevel
    {
        get;
        private set;
    }

    public long Count
    {
        get;
        private set;
    }
    #endregion
}

struct CharacterEncounter
{
    #region Static Methods
    public static long[] GetXP(CharacterEncounter characterEncounter)
    {
        var xp = new long[4];

        for (int i = 0; i < characterEncounter.Characters.Count; ++i)
        {
            var x = CondensedCharacters.GetXP(characterEncounter.Characters[i]);

            for (int j = 0; j < 4; ++j)
            {
                xp[j] += x[j];
            }
        }

        return xp;
    }
    #endregion

    #region Methods
    public CharacterEncounter(List<object> array)
    {
        Characters = new List<CondensedCharacters>(array.Count);

        for (int i = 0; i < array.Count; ++i)
        {
            var item = array[i] as Dictionary<string, object>;
            Characters.Add(new CondensedCharacters(item));
        }
    }
    #endregion

    #region Properties
    public List<CondensedCharacters> Characters
    {
        get;
        private set;
    }
    #endregion
}

struct Party
{
    #region Static Methods
    public static long GetXP(Party party, int difficulty)
    {
        long xp = 0;

        var characterXP = CharacterEncounter.GetXP(party.PCs);

        xp += characterXP[difficulty];

        xp += CreatureEncounter.GetXP(party.NPCs);

        return xp;
    }
    #endregion

    #region Methods
    public Party(Dictionary<string, object> dictionary)
    {
        PCs = new CharacterEncounter(dictionary["PCs"] as List<object>);
        NPCs = new CreatureEncounter(dictionary["NPCs"] as List<object>);
    }
    #endregion

    #region Properties
    public CharacterEncounter PCs;

    public CreatureEncounter NPCs;
    #endregion
}

struct Constants
{
    public static readonly CreatureCategory[] CreatureCategories =
    {
        new CreatureCategory(0, 10),
        new CreatureCategory(0.125f, 25),
        new CreatureCategory(0.25f, 50),
        new CreatureCategory(0.5f, 100),
        new CreatureCategory(1, 200),
        new CreatureCategory(2, 450),
        new CreatureCategory(3, 700),
        new CreatureCategory(4, 1100),
        new CreatureCategory(5, 1800),
        new CreatureCategory(6, 2300),
        new CreatureCategory(7, 2900),
        new CreatureCategory(8, 3900),
        new CreatureCategory(9, 5000),
        new CreatureCategory(10, 5900),
        new CreatureCategory(11, 7200),
        new CreatureCategory(12, 8400),
        new CreatureCategory(13, 10000),
        new CreatureCategory(14, 11500),
        new CreatureCategory(15, 13000),
        new CreatureCategory(16, 15000),
        new CreatureCategory(17, 18000),
        new CreatureCategory(18, 20000),
        new CreatureCategory(19, 22000),
        new CreatureCategory(20, 25000),
        new CreatureCategory(21, 33000),
        new CreatureCategory(22, 41000),
        new CreatureCategory(23, 50000),
        new CreatureCategory(24, 62000),
        new CreatureCategory(30, 155000),
    };

    public static readonly float[] MultiplierByEnemyCount =
    {
        1,
        1.5f,
        2,
        2,
        2,
        2,
        2.5f,
        2.5f,
        2.5f,
        2.5f,
        3,
        3,
        3,
        3,
        4,
    };

    public static readonly long[][] xpByDifficultyByPlayerLevel =
    {
        new long[]{ 25, 50, 75, 100 },
        new long[]{ 50, 100, 150, 200 },
        new long[]{ 75, 150, 225, 400 },
        new long[]{ 125, 250, 375, 500 },
        new long[]{ 250, 500, 750, 1100 },
        new long[]{ 300, 600, 900, 1400 },
        new long[]{ 350, 750, 1100, 1700 },
        new long[]{ 450, 900, 1400, 2100 },
        new long[]{ 550, 1100, 1600, 2400 },
        new long[]{ 600, 1200, 1900, 2800 },
        new long[]{ 800, 1600, 2400, 3600 },
        new long[]{ 1000, 2000, 3000, 4500 },
        new long[]{ 1100, 2200, 3400, 5100 },
        new long[]{ 1250, 2500, 3800, 5700 },
        new long[]{ 1400, 2800, 4300, 6400 },
        new long[]{ 1600, 3200, 4800, 7200 },
        new long[]{ 2000, 3900, 5900, 8800 },
        new long[]{ 2100, 4200, 6300, 9500 },
        new long[]{ 2400, 4900, 7300, 10900 },
        new long[]{ 2800, 5700, 8500, 12700 }
    };

    public static float GetMultiplier(long count)
    {
        count = Math.Max(count - 1, 0);
        count = Math.Min(count, 15);

        return MultiplierByEnemyCount[count];
    }

    public static long GetXP(long xp, long count)
    {
        float multiplier = GetMultiplier(count);

        xp = (long)(xp * multiplier + 0.5);

        return xp;
    }

    public static int ToIndex(double cr)
    {
        if (cr == 0)
        {
            return 0;
        }
        else if (cr == 0.125)
        {
            return 1;
        }
        else if (cr == 0.25)
        {
            return 2;
        }
        else if (cr == 0.5)
        {
            return 3;
        }
        else if (cr == 30.0)
        {
            return 28;
        }

        return (int)(cr + 3);
    }

    public static CreatureCategory GetCategory(double cr)
    {
        int index = ToIndex(cr);

        return CreatureCategories[index];
    }
}