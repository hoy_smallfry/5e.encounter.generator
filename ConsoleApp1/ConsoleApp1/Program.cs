﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace ConsoleApp1
{
    enum FormatType
    {
        CR,
        XP,
        Both
    }

    class Program
    {
        private static void GenerateEncounters(
            SortedDictionary<long, List<CreatureEncounter>> creatureEncountersByXP,
            List<CondensedCreatures> path = null,
            int creatureCategory = 0,
            int creatureCount = 0)
		{
            int CreatureCount = Constants.MultiplierByEnemyCount.Length; //10;
            int CreatureCategoryCount = Constants.CreatureCategories.Length; // 10;

            if (creatureCount >= CreatureCount)
            {
                return;
            }

            int multiIndex = Math.Min(creatureCount, Constants.MultiplierByEnemyCount.Length);
            
            // Get the multiplier for the enemu count.
            float multiplier = Constants.MultiplierByEnemyCount[multiIndex];

            // For each child of the current tree process
            for (; creatureCategory < CreatureCategoryCount; ++creatureCategory)
			{
                var newPath = path.GetRange(0, path.Count);

                // If the path has not seen a enemy of this categort, then:
                if (newPath.Count == 0 ||
                    newPath[newPath.Count - 1].Category !=
                    Constants.CreatureCategories[creatureCategory].ChallengeRating)
                {
                    // Add the category to this list.
                    var category = Constants.CreatureCategories[creatureCategory];
                    var condensedCreatures = new CondensedCreatures(category.ChallengeRating, 1);

                    newPath.Add(condensedCreatures);
                }
                else
                {
                    // Otherwise, we already have a creature of this category.
                    // Since we are going in order, the current category is always
                    // the last item in the array.
                    // Add one more of this category.
                    CondensedCreatures lastConsensensedCreature = newPath[newPath.Count - 1];
                    lastConsensensedCreature.Count++;
                    newPath[newPath.Count - 1] = lastConsensensedCreature;
                }

                // Construct this enemy encounter.
                var creatureEncounter = new CreatureEncounter(newPath);

                // Calculate the XP for this encounter.
                var xp = CreatureEncounter.GetXP(creatureEncounter);

                bool found = creatureEncountersByXP.TryGetValue(xp,
                    out List<CreatureEncounter> creatureEncounters);

                if (found)
                {
                    creatureEncounters.Add(creatureEncounter);
                }
                else
                {
                    creatureEncounters = new List<CreatureEncounter>(1);
                    creatureEncounters.Add(creatureEncounter);
                    creatureEncountersByXP.Add(xp, creatureEncounters);
                }

                // Build a new encounter off of this generated one.
                GenerateEncounters(creatureEncountersByXP,
                    newPath,
                    creatureCategory,
                    creatureCount + 1);
            }
        }

        private static string ToString(TimeSpan ts)
        {
            const string Format = "{0:00}:{1:00}:{2:00}.{3:00}";

            return string.Format(Format,
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10);
        }

        static void Main(string[] args)
        {
            var encounters = new SortedDictionary<long, List<CreatureEncounter>>();

            var stopWatch = new Stopwatch();
            stopWatch.Start();
            GenerateEncounters(encounters, new List<CondensedCreatures>());
            stopWatch.Stop();
            Console.WriteLine($"Built enemy encounters in {ToString(stopWatch.Elapsed)}");

            stopWatch = new Stopwatch();
            stopWatch.Start();
            string serialized = Json.Serialize(encounters, true);
            stopWatch.Stop();
            Console.WriteLine($"Built JSON serialization in {ToString(stopWatch.Elapsed)}");

            stopWatch = new Stopwatch();
            stopWatch.Start();
            using (var streamWriter = new StreamWriter("RandomEncounters.txt"))
            {
                streamWriter.Write(serialized);
            }
            stopWatch.Stop();

            Console.WriteLine($"Printed enemy encounters in {ToString(stopWatch.Elapsed)}");
        }
    }
}
