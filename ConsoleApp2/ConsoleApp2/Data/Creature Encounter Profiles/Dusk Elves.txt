{
    "0": [
        "Commoner",
    ],
    "0.125": [
        "Bandit",
    ],
    "0.5": [
        "Scout",
    ],
    "1": [
        "Spy",
    ],
    "2": [
        "Priest",
    ],
    "5": [
        "Gladiator",
    ],
    "6": [
        "Mage",
    ],
    "7": [
        "Shadow Dancer",
    ],
    "8": [
        "Assassin",
    ],
    "9": [
        "Gloom Weaver",
    ],
    "11": [
        "Soul Monger",
    ],
    "12": [
        "Archmage",
    ],
}
