{
    "0.25": [
        "Acolyte",
    ],
    "0.5": [
        "Barovian Witch",
    ],
    "2": [
        "Cult Fanatic",
        "Druid",
        "Priest",
    ],
    "3": [
        "Deathlock Wight",
    ],
    "4": [
        "Bone Naga",
        "Deathlock",
        "Dybbuk",
        "Flameskull",
    ],
    "6": [
        "Night Hag",
    ],
    "12": [
        "Arcanaloth",
        "Archmage",
    ],
    "21": [
        "Lich",
    ],
}
