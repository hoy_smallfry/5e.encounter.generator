{
    "0": [
        "Commoner",
    ],
    "0.125": [
        "Bandit",
    ],
    "0.5": [
        "Scout",
        "Thug",
    ],
    "1": [
        "Spy",
    ],
    "2": [
        "Bandit Captain",
    ],
    "5": [
        "Gladiator",
    ],
    "6": [
        "Mage",
    ],
    "8": [
        "Assassin",
    ],
    "12": [
        "Archmage",
    ],
}
