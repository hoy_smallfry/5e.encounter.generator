{
    "0": [
        "Commoner",
    ],
    "0.125": [
        "Bandit",
        "Cultist",
        "Guard",
        "Noble",
    ],
    "0.25": [
        "Acolyte",
    ],
    "0.5": [
        "Barovian Witch",
        "Scout",
        "Thug",
    ],
    "1": [
        "Spy",
    ],
    "2": [
        "Cult Fanatic",
        "Priest",
    ],
    "3": [
        "Knight",
        "Veteran",
    ],
    "4": [
        "Mind Drinker Vampire",
    ],
    "5": [
        "Gladiator",
        "Vampire Spawn",
    ],
    "6": [
        "Mage",
    ],
    "8": [
        "Assassin",
        "Blood Drinker Vampire",
    ],
}
