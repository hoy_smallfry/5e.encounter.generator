﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace ConsoleApp2
{
    class Program
    {
        struct CreatureEncounterProfile
        {
            public HashSet<int> Masks { get; set; }

            public SortedDictionary<double, List<string>> NamesByCategory { get; set; }
        }

        private static string ToString(TimeSpan ts)
        {
            const string Format = "{0:00}:{1:00}:{2:00}.{3:00}";

            return string.Format(Format,
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10);
        }

        private static int GetMask(List<double> categories)
        {
            // Create a mask that acts as fingerprint.
            int maskKey = 0;
            for (int j = 0; j < categories.Count; ++j)
            {
                var category = categories[j];

                var categoryIndex = Constants.ToIndex(category);

                int maskBit = 1 << categoryIndex;

                maskKey |= maskBit;
            }

            return maskKey;
        }

        private static int GetMask(List<CondensedCreatures> creatures)
        {
            // Create a mask that acts as fingerprint.
            int maskKey = 0;
            for (int j = 0; j < creatures.Count; ++j)
            {
                var condensedCreatures = creatures[j];

                var categoryIndex = Constants.ToIndex(condensedCreatures.Category);

                int maskBit = 1 << categoryIndex;

                maskKey |= maskBit;
            }

            return maskKey;
        }

        private static SortedDictionary<int, List<CreatureEncounter>> GetFilteredCreatureEncountersByCategories(
            Dictionary<string, object> deserialized,
            long partyXP,
            HashSet<int> masks = null)
        {
            var creatureEncountersByCategories = new SortedDictionary<int, List<CreatureEncounter>>();
            
            foreach (var pair in deserialized)
            {
                bool success = long.TryParse(pair.Key, out long xp);

                if (!success)
                {
                    continue;
                }

                if (Math.Abs(xp - partyXP) > 200)
                {
                    continue;
                }

                var creatureEncounters = pair.Value as List<object>;

                for (int i = 0; i < creatureEncounters.Count; i++)
                {
                    var creatureEncounterJson = creatureEncounters[i] as Dictionary<string, object>;

                    var creaturesListStr = creatureEncounterJson["Creatures"] as List<object>;

                    var creatureEncounter = new CreatureEncounter(creaturesListStr);

                    int maskKey = GetMask(creatureEncounter.Creatures);

                    if (!masks.Contains(maskKey))
                    {
                        continue;
                    }

                    bool found = creatureEncountersByCategories.TryGetValue(maskKey,
                        out List<CreatureEncounter> creatureEncounters2);

                    if (found)
                    {
                        creatureEncounters2.Add(creatureEncounter);
                    }
                    else
                    {
                        creatureEncounters2 = new List<CreatureEncounter>();
                        creatureEncounters2.Add(creatureEncounter);
                        creatureEncountersByCategories.Add(maskKey, creatureEncounters2);
                    }
                }
            }

            return creatureEncountersByCategories;
        }

        private static CreatureEncounterProfile GetCreatureEncounterProfiles(
            Dictionary<string, object> deserialized)
        {
            var result = new CreatureEncounterProfile()
            {
                Masks = new HashSet<int>(),
                NamesByCategory = new SortedDictionary<double, List<string>>()
            };

            var crs = new List<double>(deserialized.Count);
            foreach (var pair in deserialized)
            {
                bool success = double.TryParse(pair.Key, out double cr);

                if (!success)
                {
                    success = long.TryParse(pair.Key, out long crl);

                    if (!success)
                    {
                        continue;
                    }

                    cr = crl;
                }

                crs.Add(cr);

                var namesJson = pair.Value as List<object>;
                var names = new List<string>(namesJson.Count);
                for (int j = 0; j < namesJson.Count; ++j)
                {
                    names.Add(namesJson[j] as string);
                }

                result.NamesByCategory.Add(cr, names);
            }
            
            GenerateEncounterMasks(result.Masks, crs);

            return result;
        }

        private static void GenerateEncounterMasks(
            HashSet<int> masks,
            List<double> crs,
            int path = 0,
            int creatureCategory = 0)
        {
            if (creatureCategory >= crs.Count)
            {
                return;
            }

            // For each child of the current tree process
            for (; creatureCategory < crs.Count; ++creatureCategory)
            {
                int newPath = path;

                double cr = crs[creatureCategory];
                int index = Constants.ToIndex(cr);
                int bitFlag = 1 << index;

                newPath |= bitFlag;

                /*/
                index = masks.BinarySearch(newPath);
                if (index < 0)
                {
                    index = ~index;
                    masks.Insert(index, newPath);
                }
                //*/

                masks.Add(newPath);

                if (path != 0)
                {
                    GenerateEncounterMasks(masks,
                    crs,
                    path,
                    creatureCategory + 1);
                }

                GenerateEncounterMasks(masks,
                    crs,
                    newPath,
                    creatureCategory + 1);
            }
        }

        public static bool IsSubset(int superset, int subset)
        {
            return (superset & subset) == subset;
        }

        //private static 

        static void Main(string[] args)
        {
            var random = new Random();

            string[] partyFilenames = Directory.GetFiles("Party");

            Console.WriteLine($"Choose a party profile:");
            for (int i = 0; i < partyFilenames.Length; ++i)
            {
                Console.WriteLine($"\t{i} - {partyFilenames[i]}");
            }
            string partyIndexStr = Console.ReadLine();
            bool success = int.TryParse(partyIndexStr, out int partyIndex);
            if (!success)
            {
                return;
            }

            string partyFilename = partyFilenames[partyIndex];

            // Get the difficulty in XP for every difficulty available.
            long[] partyDifficultyXPs = new long[4];
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            using (StreamReader streamReader = new StreamReader(partyFilename))
            {
                string serializedJSON = streamReader.ReadToEnd();

                var deserialized = Json.Deserialize(serializedJSON) as Dictionary<string, object>;

                var partyWorth = new Party(deserialized);

                for (int i = 0; i < 4; ++i)
                {
                    partyDifficultyXPs[i] = Party.GetXP(partyWorth, i);
                }
            }
            stopWatch.Stop();
            Console.WriteLine($"Read party encounters in {ToString(stopWatch.Elapsed)}");

            Console.WriteLine($"Choose a difficulty:");
            Console.WriteLine($"\t0 - Easy - {partyDifficultyXPs[0]} XP");
            Console.WriteLine($"\t1 - Medium - {partyDifficultyXPs[1]} XP");
            Console.WriteLine($"\t2 - Hard - {partyDifficultyXPs[2]} XP");
            Console.WriteLine($"\t3 - Lethal - {partyDifficultyXPs[3]} XP");

            string difficultyStr = Console.ReadLine();
            success = int.TryParse(difficultyStr, out int difficulty);

            if (!success)
            {
                return;
            }

            long partyDifficultyXP = partyDifficultyXPs[difficulty];
            Console.WriteLine($"The party's XP at difficulty {difficulty} is {partyDifficultyXP} XP");

            stopWatch = new Stopwatch();
            stopWatch.Start();
            string[] creatureProfilesFilenames = Directory.GetFiles("Creature Encounter Profiles");
            Console.WriteLine($"Choose a creature profile:");
            for (int i = 0; i < creatureProfilesFilenames.Length; ++i)
            {
                Console.WriteLine($"\t{i} - {creatureProfilesFilenames[i]}");
            }
            Console.WriteLine($"\t{creatureProfilesFilenames.Length} - Random");
            stopWatch.Stop();
            string creatureIndexStr = Console.ReadLine();
            success = int.TryParse(creatureIndexStr, out int creatureIndex);
            if (!success)
            {
                return;
            }

            if (creatureProfilesFilenames.Length == creatureIndex)
            {
                creatureIndex = random.Next(0, creatureProfilesFilenames.Length);
            }



            string creatureProfileFilename = creatureProfilesFilenames[creatureIndex];
            CreatureEncounterProfile creatureProfile;
            using (StreamReader streamReader = new StreamReader(creatureProfileFilename))
            {
                string serializedJSON = streamReader.ReadToEnd();

                var deserialized = Json.Deserialize(serializedJSON) as Dictionary<string, object>;

                creatureProfile = GetCreatureEncounterProfiles(deserialized);
            }



            SortedDictionary<int, List<CreatureEncounter>> creatureEncountersByCategories;
            stopWatch = new Stopwatch();
            stopWatch.Start();
            using (StreamReader streamReader = new StreamReader("RandomEncounters.txt"))
            {
                string serializedJSON = streamReader.ReadToEnd();

                var deserialized = Json.Deserialize(serializedJSON) as Dictionary<string, object>;

                creatureEncountersByCategories = GetFilteredCreatureEncountersByCategories(
                    deserialized, partyDifficultyXP, creatureProfile.Masks);
            }
            stopWatch.Stop();
            Console.WriteLine($"Read enemy encounters in {ToString(stopWatch.Elapsed)}");



            foreach (var pair in creatureEncountersByCategories)
            {
                foreach (var encounter in pair.Value)
                {
                    var categorizedNames = new List<string>(encounter.Creatures.Count);
                    foreach (var condensed in encounter.Creatures)
                    {
                        List<string> names = creatureProfile.NamesByCategory[condensed.Category];

                        int nameIndex = random.Next(0, names.Count);
                        string name = names[nameIndex];

                        categorizedNames.Add(name);
                    }

                    long encounterXP = CreatureEncounter.GetXP(encounter);
                    long diffXP = encounterXP - partyDifficultyXP;
                    string diffXPString;
                    if (diffXP > 0)
                    {
                        diffXPString = $"{Math.Abs(diffXP)} XP above difficulty";
                    }
                    else
                    {
                        diffXPString = $"{Math.Abs(diffXP)} XP below difficulty";
                    }

                    string encounterXPString = $"{encounterXP} ({diffXPString})";

                    string message = CreatureEncounter.ToString(encounter, categorizedNames, encounterXPString);
                    Console.WriteLine(message);
                }
            }

            Console.ReadLine();
        }
    }
}
